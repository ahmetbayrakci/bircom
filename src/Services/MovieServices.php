<?php


namespace App\Services;


use App\Command\InsertDataCommand;
use App\Command\RetrieveDataCommand;
use App\Entity\Movies;
use Symfony\Component\Console\Input\ArrayInput;
use Symfony\Component\Console\Output\BufferedOutput;
use Symfony\Component\Console\Output\NullOutput;
use Doctrine\ORM\EntityManagerInterface;

class MovieServices
{
    private $doctrine;

    public function __construct(EntityManagerInterface $doctrine)
    {
        $this->doctrine = $doctrine;
    }

    public function insertAndFetchData()
    {

        $command = new RetrieveDataCommand($this->doctrine);
        $input = new ArrayInput(array());

        $output = new BufferedOutput();
        $command->run($input, $output);

        $movieContent = json_decode($output->fetch(), true);

        foreach ($movieContent as $item) {
            $this->addMoviesWithCommand($item["show"]["name"], $item["show"]["language"], $item["show"]["id"], $item["show"]["premiered"]);
        }

        return $this->getDataFromDatabase();
    }


    public function getDataFromDatabase()
    {

        return $this->doctrine->getRepository(Movies::class)->findAll();
    }

    public function addMoviesWithCommand($name, $lang, $movie_id, $premiered)
    {
        $command = new InsertDataCommand($this->doctrine);
        $input = new ArrayInput(array(
            'name' => $name,
            'language' => $lang,
            'movieId' => $movie_id,
            'premiered' => $premiered
        ));

        $output = new NullOutput();
        $command->run($input, $output);

    }

    // I confused about this situation and i added two different method for insert data to database
    public function addMovies($name, $lang, $movie_id, $premiered)
    {
        if (!$this->doctrine->getRepository(Movies::class)->findBy(["movie_id" => $movie_id])) {
            $movies = new Movies();
            $movies->setName($name);
            $movies->setLanguage($lang);
            $movies->setMovieId($movie_id);
            $movies->setPremiered($premiered);

            $this->doctrine->persist($movies);
            $this->doctrine->flush();
        }
    }
}