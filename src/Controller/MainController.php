<?php

namespace App\Controller;


use App\Services\MovieServices;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;


class MainController extends AbstractController
{
    /**
     * @Route("/main", name="main")
     */
    public function index(): Response
    {
        $service = new MovieServices($this->getDoctrine()->getManager());



        return $this->render('index.html.twig', [
            'content' => $service->insertAndFetchData()
        ]);

    }

}
