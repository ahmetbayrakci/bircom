<?php


namespace App\Command;

use App\Entity\Movies;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class InsertDataCommand extends Command
{

    protected $entityManager;
    protected static $defaultName = 'app:insert-data';

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
        parent::__construct();
    }

    protected function configure()
    {
        $this->setName('insertData')->setDescription("Getting data from API endpoint")
            ->addArgument('name', InputArgument::REQUIRED, 'Movie Name')
            ->addArgument('language', InputArgument::REQUIRED, 'Movie Language ? ')
            ->addArgument('movieId', InputArgument::REQUIRED, 'Movie ID')
            ->addArgument('premiered', InputArgument::REQUIRED, 'Movie Premiered');
    }



    protected function execute(InputInterface $input, OutputInterface $output)
    {
        if(!$this->entityManager->getRepository(Movies::class)->findBy(["movie_id" =>$input->getArgument('movieId')])){
            $movies = new Movies();
            $movies->setName($input->getArgument('name'));
            $movies->setLanguage($input->getArgument('language'));
            $movies->setMovieId($input->getArgument('movieId'));
            $movies->setPremiered($input->getArgument('premiered'));

            $this->entityManager->persist($movies);
            $this->entityManager->flush();
        }

        return Command::SUCCESS;


    }

}