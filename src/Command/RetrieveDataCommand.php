<?php


namespace App\Command;

use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\HttpClient\HttpClient;

class RetrieveDataCommand extends Command
{

    protected $entityManager;
    protected static $defaultName = 'app:get-data';

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
        parent::__construct();
    }

    protected function configure()
    {
        $this->setName('getApi')->setDescription("Getting data from API endpoint");
    }



    protected function execute(InputInterface $input, OutputInterface $output)
    {

        $client = HttpClient::create();
        $response = $client->request('GET', 'http://api.tvmaze.com/search/shows?q=comedy');
        $content = $response->getContent();

        $output->writeln($content);

        return Command::SUCCESS;


    }

}